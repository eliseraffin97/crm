package Entity;

import javafx.scene.control.Button;

public class Representant extends Personne {

	private int numRepresentant;
	private String login;
	private String password;
	private Button modifier;
	private Button supprimer;
	
	public Representant(String nom, String prenom, int num_representant, String login, String password) {
		super(nom, prenom);
		this.numRepresentant = num_representant;
		this.login = login;
		this.password = password;
	}

	public Representant(String nom, String prenom, int num_representant, String login, String password, Button modifier, Button supprimer) {
		super(nom, prenom);
		this.numRepresentant = num_representant;
		this.login = login;
		this.password = password;
		this.setModifier(modifier);
		this.setSupprimer(supprimer);
	}

	
	public int getNumRepresentant() {
		return numRepresentant;
	}

	public void setNumRepresentant(int num_representant) {
		this.numRepresentant = num_representant;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Button getModifier() {
		return modifier;
	}

	public void setModifier(Button modifier) {
		this.modifier = modifier;
	}

	public Button getSupprimer() {
		return supprimer;
	}

	public void setSupprimer(Button supprimer) {
		this.supprimer = supprimer;
	}
	
	
}
