package Entity;

public class Client extends Personne{

	private int numClient;
	private String raisonSociale;
	private String adressePostale;
	
	public Client(String nom, String prenom, int num_client, String raison_sociale, String adresse_postale) {
		super(nom, prenom);
		this.numClient = num_client;
		this.raisonSociale = raison_sociale;
		this.adressePostale = adresse_postale;
	}

	public int getNumClient() {
		return numClient;
	}

	public void setNumClient(int num_client) {
		this.numClient = num_client;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}
	
	public void setRaisonSociale(String raison_sociale) {
		this.raisonSociale = raison_sociale;
	}

	public String getAdressePostale() {
		return adressePostale;
	}

	public void setAdressePostale(String adresse_postale) {
		this.adressePostale = adresse_postale;
	}

		@Override
	public String toString() {
		return "Client n�" + numClient + " - " + this.getNom() + " " + this.getPrenom() + " - " + raisonSociale;
	}

}
