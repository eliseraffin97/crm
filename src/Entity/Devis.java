package Entity;

import java.util.Date;

import javafx.scene.control.Button;


public class Devis {
	
	private int numDevis;
	private Date dateCreation;
	private Date dateFinValidite;
	private double totalHt;
	private double totalTva;
	private double totalTtc;
	private int status;
	private String statut;
	private Client client;
	private int numClient;
	private int numRepresentant;
	private String nomClient;
	private Button supprimer;

	public Devis(int num, Date dateDebut, Date dateFin, double totalHT, double totalTVA, double totalTTC, int statusDevis, int numClient, int numRepresentant) {
		this.numDevis = num;
		this.dateCreation = dateDebut;
		this.dateFinValidite = dateFin;
		this.totalHt = totalHT;
		this.totalTva = totalTVA;
		this.totalTtc = totalTTC;
		this.status = statusDevis;
		if (statusDevis == 0) {
			this.setStatut("En cours");
		}else if (statusDevis == 1) {
			this.setStatut("Command�");
		} else {
			this.setStatut("Obsol�te");
		}
		this.numClient = numClient;
		this.setNumRepresentant(numRepresentant);
	}
	
	public Devis(int num, Date dateDebut, Date dateFin, double totalHT, double totalTVA, double totalTTC, int statusDevis, int numClient, Client client, int numRepresentant) {
		this.numDevis = num;
		this.dateCreation = dateDebut;
		this.dateFinValidite = dateFin;
		this.totalHt = totalHT;
		this.totalTva = totalTVA;
		this.totalTtc = totalTTC;
		this.status = statusDevis;
		if (statusDevis == 0) {
			this.setStatut("En cours");
		}else if (statusDevis == 1) {
			this.setStatut("Command�");
		} else {
			this.setStatut("Obsol�te");
		}
		this.numClient = numClient;
		this.client = client;
		this.setNomClient(client.getNom() + " " + client.getPrenom() + " - " + client.getRaisonSociale());
		this.setNumRepresentant(numRepresentant);
	}
	
	public Devis(int num, Date dateDebut, Date dateFin, double totalHT, double totalTVA, double totalTTC, int statusDevis, int numClient, Client client, int numRepresentant, Button supprimer) {
		this.numDevis = num;
		this.dateCreation = dateDebut;
		this.dateFinValidite = dateFin;
		this.totalHt = totalHT;
		this.totalTva = totalTVA;
		this.totalTtc = totalTTC;
		this.status = statusDevis;
		if (statusDevis == 0) {
			this.setStatut("En cours");
		}else if (statusDevis == 1) {
			this.setStatut("Command�");
		} else {
			this.setStatut("Obsol�te");
		}
		this.numClient = numClient;
		this.client = client;
		this.setNomClient(client.getNom() + " " + client.getPrenom() + " - " + client.getRaisonSociale());
		this.setNumRepresentant(numRepresentant);
		this.setSupprimer(supprimer);
	}
	
	public int getNumDevis() {
		return numDevis;
	}

	public void setNumDevis(int numDevis) {
		this.numDevis = numDevis;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateFinValidite() {
		return dateFinValidite;
	}

	public void setDateFinValidite(Date dateFinValidite) {
		this.dateFinValidite = dateFinValidite;
	}

	public double getTotalHt() {
		return totalHt;
	}

	public void setTotalHt(double totalHt) {
		this.totalHt = totalHt;
	}

	public double getTotalTva() {
		return totalTva;
	}

	public void setTotalTva(double totalTva) {
		this.totalTva = totalTva;
	}

	public double getTotalTtc() {
		return totalTtc;
	}

	public void setTotalTtc(double totalTtc) {
		this.totalTtc = totalTtc;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public int getNumClient() {
		return numClient;
	}

	public void setNumClient(int numClient) {
		this.numClient = numClient;
	}

	public int getNumRepresentant() {
		return numRepresentant;
	}

	public void setNumRepresentant(int numRepresentant) {
		this.numRepresentant = numRepresentant;
	}

	public Button getSupprimer() {
		return supprimer;
	}

	public void setSupprimer(Button supprimer) {
		this.supprimer = supprimer;
	}

	
	
	
}
