package Entity;

import java.util.Date;

public class Relance {

	private int num;
	private int numDevis;
	private Date dateRelance;
	
	public Relance(int num, int num_devis, Date date_relance) {
		this.num = num;
		this.numDevis = num_devis;
		this.dateRelance = date_relance;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getNumDevis() {
		return numDevis;
	}

	public void setNumDevis(int num_devis) {
		this.numDevis = num_devis;
	}

	public Date getDateRelance() {
		return dateRelance;
	}

	public void setDateRelance(Date date_relance) {
		this.dateRelance = date_relance;
	}
	
	@Override
	public String toString() {
		return "Relance le : " + this.getDateRelance().toString();
	}
	
}
