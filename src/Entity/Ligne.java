package Entity;

import javafx.scene.control.Button;

public class Ligne {

	private int numLigne;
	private int numDevis;
	private String nomArticle;
	private int quantite;
	private double prixUnitaire;
	private double totalHt;
	private double totalTva;
	private double totalTtc;
	private Button supprimer;
	
	public Ligne(int numLigne, int numDevis, String nomArticle, int quant, double prixUnitaire, double totalHT, double totalTVA, double totalTTC) {
		this.numLigne = numLigne;
		this.numDevis = numDevis;
		this.nomArticle = nomArticle;
		this.quantite = quant;
		this.prixUnitaire = prixUnitaire;
		this.totalHt = totalHT;
		this.totalTva = totalTVA;
		this.totalTtc = totalTTC;
	}
	
	public Ligne(int numLigne, int numDevis, String nomArticle, int quant, double prixUnitaire, double totalHT, double totalTVA, double totalTTC, Button supprimer) {
		this.numLigne = numLigne;
		this.numDevis = numDevis;
		this.nomArticle = nomArticle;
		this.quantite = quant;
		this.prixUnitaire = prixUnitaire;
		this.totalHt = totalHT;
		this.totalTva = totalTVA;
		this.totalTtc = totalTTC;
		this.setSupprimer(supprimer);
	}

	public int getNumLigne() {
		return numLigne;
	}

	public void setNumLigne(int num_ligne) {
		this.numLigne = num_ligne;
	}

	public int getNumDevis() {
		return numDevis;
	}

	public void setNumDevis(int num_devis) {
		this.numDevis = num_devis;
	}

	public String getNomArticle() {
		return nomArticle;
	}

	public void setNomArticle(String nom_article) {
		this.nomArticle = nom_article;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public double getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(double prix_unitaire) {
		this.prixUnitaire = prix_unitaire;
	}

	public double getTotalHt() {
		return totalHt;
	}

	public void setTotalHt(double total_ht) {
		this.totalHt = total_ht;
	}

	public double getTotalTva() {
		return totalTva;
	}

	public void setTotalTva(double total_tva) {
		this.totalTva = total_tva;
	}

	public double getTotalTtc() {
		return totalTtc;
	}

	public void setTotalTtc(double total_ttc) {
		this.totalTtc = total_ttc;
	}

	@Override
	public String toString() {
		return "Ligne [num_ligne=" + numLigne + ", num_devis=" + numDevis + ", nom_article=" + nomArticle
				+ ", quantite=" + quantite + ", prix_unitaire=" + prixUnitaire + ", total_ht=" + totalHt
				+ ", total_tva=" + totalTva + ", total_ttc=" + totalTtc + "]";
	}

	public Button getSupprimer() {
		return supprimer;
	}

	public void setSupprimer(Button supprimer) {
		this.supprimer = supprimer;
	}
	
	

}
