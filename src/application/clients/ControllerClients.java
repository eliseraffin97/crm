package application.clients;

import java.sql.SQLException;

import org.w3c.dom.events.MouseEvent;

import Entity.Client;
import Entity.Devis;
import Services.Services;
import application.Controller;
import application.devis.ControllerDevis;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ControllerClients extends Controller {

	// Page liste clients
	@FXML
	private Pane paneClient;
	@FXML
	private TableView<Client> listeClients;
	@FXML
	private TableColumn<Client, String> idTabClient;
	@FXML
	private TableColumn<Client, String> nomTabClient;
	@FXML
	private TableColumn<Client, String> prenomTabClient;
	@FXML
	private TableColumn<Client, String> raisonSocialeTabClient;
	@FXML
	private Button nouveauClient;

	// Page ajouter client
	@FXML
	private Pane paneAjoutClient;
	@FXML
	private Button enregistrerClient;
	@FXML
	private Button btnAnnuler;
	@FXML
	private Label numClient;
	@FXML
	private TextField nom;
	@FXML
	private TextField prenom;
	@FXML
	private TextField raisonSociale;
	@FXML
	private TextField typeRue;
	@FXML
	private TextField nomRue;
	@FXML
	private TextField numRue;
	@FXML
	private TextField codePostal;
	@FXML
	private TextField ville;
	@FXML
	private Label erreurNumRue;
	@FXML
	private Label erreurCodePostal;
	@FXML
	private Label erreurFormulaireVide;

	// Page voir client
	@FXML
	private Label nomPrenom;
	@FXML
	private Label raisonSocialeAffichage;
	@FXML
	private Label adresse;
	@FXML
	private TextField modifierNom;
	@FXML
	private TextField modifierPrenom;
	@FXML
	private TextField modifierRaisonSociale;
	@FXML
	private TextArea modifierAdresse;
	@FXML
	private Button enregistrerModif;
	@FXML
	private Button annulerModif;
	@FXML
	private Button modifier;
	@FXML
	private Button supprimer;
	@FXML
	private Pane voirClientPane;

	boolean formOk;

	@FXML
	BorderPane panneauPrincipal;

	// Affichage formulaire cr�ation client
	public void ajoutClient(ActionEvent e) {
		chargerPage("ajouterClient.fxml", (BorderPane) this.paneClient.getParent());
	}
	
	// Annule la cr�ation du client en revenant � la liste des clients
	public void annulerCreation() {
		this.panneauPrincipal = (BorderPane) this.paneAjoutClient.getParent();
		chargerPage("listeClients.fxml", this.panneauPrincipal);
		recupererDonneesClients(this.panneauPrincipal);
	}

	// Enregistrement du nouveau client en base de donn�es
	public void enregistrerClient(ActionEvent e) {
		Services services = new Services();
		this.formOk = true;
		this.erreurCodePostal.setText("");
		this.erreurNumRue.setText("");
		this.erreurFormulaireVide.setText("");
		checkClient();
		if (this.formOk) {
			String nom = this.nom.getText();
			String prenom = this.prenom.getText();
			String raisonSociale = this.raisonSociale.getText();
			String adresse = this.numRue.getText() + " " + this.typeRue.getText() + " " + this.nomRue.getText() + " "
					+ this.codePostal.getText() + " " + this.ville.getText();
			try {
				services.addClient(nom, prenom, raisonSociale, adresse);
			} catch (ClassNotFoundException | SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			this.panneauPrincipal = (BorderPane) this.paneAjoutClient.getParent();
			chargerPage("listeClients.fxml", this.panneauPrincipal);
			recupererDonneesClients(this.panneauPrincipal);

		}
	}

	// Check des informations saisies dans le formulaire de cr�ation - Gestion des
	// �ventuelles erreurs
	private void checkClient() {
		try {
			Integer.parseInt(this.numRue.getText());
		} catch (Exception e) {
			this.erreurNumRue.setText("Veuillez saisir un nombre");
			this.formOk = false;
		}
		try {
			Integer.parseInt(this.codePostal.getText());
			if (this.codePostal.getLength() != 5) {
				this.erreurCodePostal.setText("Code postal incorrect");
				this.formOk = false;
			}
		} catch (Exception e) {
			this.erreurCodePostal.setText("Veuillez saisir un nombre");
			this.formOk = false;
		}
		if (this.nom.getText().length() == 0 || this.prenom.getText().length() == 0
				|| this.raisonSociale.getText().length() == 0 || this.typeRue.getText().length() == 0
				|| this.nomRue.getText().length() == 0 || this.ville.getText().length() == 0) {
			this.erreurFormulaireVide.setText("Certains champs sont manquants. Tous les champs sont obligatoires");
			this.formOk = false;
		}
	}

	// CHarger les donn�es des clients dans le tableau
	public void recupererDonneesClients(BorderPane panneauPrincipal) {
		this.panneauPrincipal = panneauPrincipal;
		Services services = new Services();
		this.paneClient = (Pane) panneauPrincipal.getCenter();
		this.listeClients = (TableView<Client>) this.paneClient.getChildren().get(0);
		this.idTabClient = (TableColumn<Client, String>) this.listeClients.getColumns().get(0);
		this.nomTabClient = (TableColumn<Client, String>) this.listeClients.getColumns().get(1);
		this.prenomTabClient = (TableColumn<Client, String>) this.listeClients.getColumns().get(2);
		this.raisonSocialeTabClient = (TableColumn<Client, String>) this.listeClients.getColumns().get(3);
		this.idTabClient.setCellValueFactory(new PropertyValueFactory<Client, String>("numClient"));
		this.nomTabClient.setCellValueFactory(new PropertyValueFactory<Client, String>("nom"));
		this.prenomTabClient.setCellValueFactory(new PropertyValueFactory<Client, String>("prenom"));
		this.raisonSocialeTabClient.setCellValueFactory(new PropertyValueFactory<Client, String>("raisonSociale"));
		ObservableList<Client> data = FXCollections.observableArrayList();
		try {
			for (Client client : services.getAllClients()) {
				data.add(client);
			}
			this.listeClients.setItems(data);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Affichage du client selectionn�
	public void voirClient() {
		Client client = this.listeClients.getSelectionModel().getSelectedItem();
		try {

			// Initalisation des composants
			this.panneauPrincipal = (BorderPane) this.paneClient.getParent();
			chargerPage("voirClient.fxml", this.panneauPrincipal);
			this.voirClientPane = (Pane) this.panneauPrincipal.getCenter();
			this.nomPrenom = (Label) this.voirClientPane.getChildrenUnmodifiable().get(4);
			this.raisonSocialeAffichage = (Label) this.voirClientPane.getChildrenUnmodifiable().get(5);
			this.adresse = (Label) this.voirClientPane.getChildrenUnmodifiable().get(6);
			this.numClient = (Label) this.voirClientPane.getChildrenUnmodifiable().get(1);
			this.modifierNom = (TextField) this.voirClientPane.getChildrenUnmodifiable().get(7);
			this.modifierPrenom = (TextField) this.voirClientPane.getChildrenUnmodifiable().get(10);
			this.modifierRaisonSociale = (TextField) this.voirClientPane.getChildrenUnmodifiable().get(8);
			this.modifierAdresse = (TextArea) this.voirClientPane.getChildrenUnmodifiable().get(9);
			this.enregistrerModif = (Button) this.voirClientPane.getChildrenUnmodifiable().get(11);
			this.annulerModif = (Button) this.voirClientPane.getChildrenUnmodifiable().get(12);
			this.modifier = (Button) this.voirClientPane.getChildrenUnmodifiable().get(3);
			this.supprimer = (Button) this.voirClientPane.getChildrenUnmodifiable().get(2);
			this.nomPrenom.setText(client.getNom() + " " + client.getPrenom());
			this.raisonSocialeAffichage.setText(client.getRaisonSociale());
			this.adresse.setText(client.getAdressePostale());
			this.numClient.setText(String.valueOf(client.getNumClient()));
			this.modifierNom.setText(client.getNom());
			;
			this.modifierPrenom.setText(client.getPrenom());
			this.modifierRaisonSociale.setText(client.getRaisonSociale());
			;
			this.modifierAdresse.setText(client.getAdressePostale());

			Services s = new Services();
			int count = 0;
			for (Devis devis : s.getAllDevis()) {
				if (devis.getNumClient() == client.getNumClient()) {
					count++;
				}
			}
			// Si le client a des devis associ�s on ne peut pas le supprimer
			if (count != 0) {
				this.supprimer.setDisable(true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Chargement du formulaire de modification du client
	public void modifierClient() {

		this.modifier.setVisible(false);
		this.modifier.setDisable(true);
		this.supprimer.setVisible(false);
		this.supprimer.setDisable(true);
		this.enregistrerModif.setDisable(false);
		this.enregistrerModif.setVisible(true);
		this.annulerModif.setDisable(false);
		this.annulerModif.setVisible(true);

		this.nomPrenom.setVisible(false);
		this.raisonSocialeAffichage.setVisible(false);
		this.adresse.setVisible(false);

		this.modifierNom.setVisible(true);
		this.modifierPrenom.setVisible(true);
		this.modifierRaisonSociale.setVisible(true);
		this.modifierAdresse.setVisible(true);
		this.modifierNom.setDisable(false);
		this.modifierPrenom.setDisable(false);
		this.modifierRaisonSociale.setDisable(false);
		this.modifierAdresse.setDisable(false);
	}

	// Suppression en base du client
	public void supprimerClient() {
		Services services = new Services();
		try {
			services.deleteClient(Integer.parseInt(this.numClient.getText()));
			fermerFenetreVoirClient((BorderPane) this.supprimer.getParent().getParent());
		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Ajouter en base le nouveau client
	public void enregistrerModification() {
		Services services = new Services();
		try {
			services.updateClient(Integer.parseInt(this.numClient.getText()), this.modifierNom.getText(),
					this.modifierPrenom.getText(), this.modifierRaisonSociale.getText(),
					this.modifierAdresse.getText());
			Scene stageLogin = (Scene) this.numClient.getScene();
			fermerFenetreVoirClient((BorderPane) this.enregistrerModif.getParent().getParent());
		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Revenir a l'affichage seulement du client
	public void annulerModification() {
		fermerFenetreVoirClient((BorderPane) this.annulerModif.getParent().getParent());
	}

	// Revenir � la liste des clients
	public void fermerFenetreVoirClient(BorderPane panneau) {
		chargerPage("listeClients.fxml", panneau);
		recupererDonneesClients(panneau);
	}

}
