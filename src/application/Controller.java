package application;

import application.clients.ControllerClients;
import application.devis.ControllerDevis;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Entity.Representant;
import Services.Services;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Controller implements Initializable{

	Services services;

	// Page principale avec menu
	@FXML private Rectangle boutonDevis;
	@FXML private Rectangle boutonClients;
	@FXML private ImageView deconnexion;
	@FXML private Label representant;
	@FXML private Label idSession;

	// Page connexion
	@FXML private Button connexion;
	@FXML private Label erreurConnexion;
	@FXML private TextField username;
	@FXML private PasswordField password;

	// Page root
	@FXML private ImageView boutonDeco;
	@FXML private TableView<Representant> tableRepresentants; // TODO mettre tableView<Representant>
	@FXML private TableColumn<Representant, SimpleStringProperty> colonneNom;
	@FXML private TableColumn<Representant, SimpleStringProperty> colonnePrenom;
	@FXML private TableColumn<Representant, SimpleStringProperty> colonneId;
	@FXML private TableColumn<Representant, SimpleStringProperty> colonneMdp;
	@FXML private TableColumn<Representant, Button> colonneModifier;
	@FXML private TableColumn<Representant, Button> colonneSupprimer;
	@FXML private TextField nom;
	@FXML private TextField prenom;
	@FXML private TextField identifiant;
	@FXML private TextField mdp;
	@FXML private Button soumettreRepresentant;


	@FXML public BorderPane panneauPrincipal;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.services = new Services();
	}
	
	
	// Gestion des droits de connexion
	public void connexion(ActionEvent a) {
		try {
			Stage stageLogin = (Stage) this.connexion.getScene().getWindow();
			FXMLLoader fxmlLoader = new FXMLLoader();
			String id = this.username.getText();
			String pswd = this.password.getText();
			boolean connexionOk = false;

			if (id.equals("root") && pswd.equals("root")) {
				afficherRoot(fxmlLoader);
				connexionOk = true;
				stageLogin.close();
			} else {
				ArrayList<Representant> reps = this.services.getAllRepresentants();
				for (Representant rep: reps) {
					if (rep.getLogin().equals(id) && rep.getPassword().equals(pswd)) {
						fxmlLoader.setLocation(getClass().getResource("vue.fxml"));
						BorderPane scene = (BorderPane) fxmlLoader.load();
						this.representant = (Label) ((Pane) scene.getLeft()).getChildren().get(3);
						this.representant.setText(rep.getPrenom() + " " + rep.getNom());
						this.idSession = (Label) ((Pane) scene.getLeft()).getChildren().get(11);
						this.idSession.setText(Integer.toString(rep.getNumRepresentant()));
						Stage stage = new Stage();
						stage.setTitle("Crm");
						stage.setScene(new Scene(scene));
						stage.show();
						FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("devis/listeDevis.fxml"));
						Pane root1 = (Pane) fxmlLoader2.load();
						scene.setCenter(root1);
						connexionOk = true;
						ControllerDevis controllerDevis = new ControllerDevis();
						controllerDevis.recupererDonneesDevis(scene, rep.getNumRepresentant());
						stageLogin.close();
					}
				}
				if (!connexionOk) {
					this.erreurConnexion.setText("Vos identifiants sont incorrects.");
				} else {
					stageLogin.close();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void deconnexion(MouseEvent e) {
		try {
			Stage pagePrincipale = (Stage) this.deconnexion.getScene().getWindow();
			pagePrincipale.close();
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(getClass().getResource("login.fxml"));
			Pane scene = (Pane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Connexion");
			stage.setScene(new Scene(scene));
			stage.show();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}


	// Charge la page avec la liste des devis suite � un clique dans le menu
	public void menuDevis(MouseEvent e) {
		chargerPage("devis/listeDevis.fxml", this.panneauPrincipal);
		ControllerDevis controllerDevis = new ControllerDevis();
		controllerDevis.recupererDonneesDevis(this.panneauPrincipal, Integer.parseInt(this.idSession.getText()));
		this.boutonClients.getStyleClass().clear();
		this.boutonDevis.getStyleClass().clear();
		this.boutonClients.getStyleClass().add("bouton-non-selectionne");
		this.boutonDevis.getStyleClass().add("bouton-selectionne");
	}

	// Charge la page avec la liste des clients suite � un clique dans le menu
	public void menuClients() {
		chargerPage("clients/listeClients.fxml", this.panneauPrincipal);
		ControllerClients controllerClients = new ControllerClients();
		controllerClients.recupererDonneesClients(this.panneauPrincipal);
		this.boutonClients.getStyleClass().clear();
		this.boutonDevis.getStyleClass().clear();
		this.boutonDevis.getStyleClass().add("bouton-non-selectionne");
		this.boutonClients.getStyleClass().add("bouton-selectionne");
	}


	// Charge une page donn�e
	protected void chargerPage(String string, BorderPane panneau) {
		try {
			FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource(string));
			Pane root1 = (Pane) fxmlLoader2.load();
			panneau.setCenter(root1);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	
	// Gestion des repr�sentants


	// Page de la gestion des repr�sentants
	private void afficherRoot(FXMLLoader fxmlLoader) {
		fxmlLoader.setLocation(getClass().getResource("root.fxml"));
		Pane scene;
		try {
			scene = (Pane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Administration");
			stage.setScene(new Scene(scene));
			stage.show();
			this.tableRepresentants = (TableView<Representant>) scene.getChildren().get(1);
			this.tableRepresentants.setEditable(true);

			// Initialisation du tableau
			for (TableColumn column: this.tableRepresentants.getColumns()) {
				if (column.getId().equals("colonneId")) {
					this.colonneId = column;
					this.colonneId.setCellValueFactory(new PropertyValueFactory<Representant, SimpleStringProperty>("login"));
				}
				if (column.getId().equals("colonneNom")) {
					this.colonneNom = column;
					this.colonneNom.setCellValueFactory(new PropertyValueFactory<Representant, SimpleStringProperty>("nom"));

				}
				if (column.getId().equals("colonnePrenom")) {
					this.colonnePrenom = column;
					this.colonnePrenom.setCellValueFactory(new PropertyValueFactory<Representant, SimpleStringProperty>("prenom"));

				}
				if (column.getId().equals("colonneMdp")) {
					this.colonneMdp = column;
					this.colonneMdp.setCellValueFactory(new PropertyValueFactory<Representant, SimpleStringProperty>("password"));

				}
				if (column.getId().equals("colonneModifier")) {
					this.colonneModifier = column;
					this.colonneModifier.setCellValueFactory(new PropertyValueFactory<Representant, Button>("modifier"));
					this.colonneModifier.setStyle("-fx-alignment: CENTER;");
				}
				if (column.getId().equals("colonneSupprimer")) {
					this.colonneSupprimer = column;
					this.colonneSupprimer.setCellValueFactory(new PropertyValueFactory<Representant, Button>("supprimer"));
					this.colonneSupprimer.setStyle("-fx-alignment: CENTER;");
				}
			}
			chargerDonneesRepresentants();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Vide le formulaire de cr�ation/modification
	public void annulerSaisie() {
		this.nom.setText("");
		this.prenom.setText("");
		this.identifiant.setText("");
		this.mdp.setText("");
	}
	

	public void ajouterRepresentant() {
		try {
			int id = this.services.addRepresentant(this.nom.getText(), this.prenom.getText(), this.identifiant.getText(), this.mdp.getText());
			Representant newRepresentant = new Representant(this.nom.getText(), this.prenom.getText(), id, this.identifiant.getText(), this.mdp.getText());
			this.tableRepresentants.getItems().add(creerLigneRepresentant(newRepresentant));
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	private void chargerDonneesRepresentants() {
		try {
			ObservableList<Representant> data = FXCollections.observableArrayList();
			for (Representant representant: services.getAllRepresentants()) {
				data.add(creerLigneRepresentant(representant));
			}
			this.tableRepresentants.setItems(data);

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Representant creerLigneRepresentant(Representant representant) {
		Image imgEdit = new Image("img/logo-edit.png");
		ImageView viewEdit = new ImageView(imgEdit);
		viewEdit.resize(15, 15);
		Image imgDelete = new Image("img/logo-delete.png");
		ImageView viewDelete = new ImageView(imgDelete);
		viewDelete.resize(15, 15);
		Button modifier = new Button("", viewEdit);
		Button supprimer = new Button("", viewDelete);
		modifier.setStyle("-fx-background-color:  DODGERBLUE;");
		supprimer.setStyle("-fx-background-color:  DODGERBLUE;");
		modifier.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				modifierRepresentant(representant);
			}
		});
		supprimer.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent a) {
				supprimerRepresentant(representant);
			}
		});
		return new Representant(representant.getNom(), representant.getPrenom(),
				representant.getNumRepresentant(), representant.getLogin(), representant.getPassword(), modifier, supprimer);
	}

	protected void supprimerRepresentant(Representant representant) {
		try {
			for (int i = 0; i < this.tableRepresentants.getItems().size(); i++) {
				Representant rep = this.tableRepresentants.getItems().get(i);
				if (rep.getNumRepresentant() == representant.getNumRepresentant()) {
					this.tableRepresentants.getItems().remove(i);
				}
			}
			this.services.deleteRepresentant(representant.getNumRepresentant());
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void modifierRepresentant(Representant representant) {
		Pane panneauRoot = (Pane) this.tableRepresentants.getParent();
		this.nom = (TextField) panneauRoot.getChildrenUnmodifiable().get(7);
		this.prenom = (TextField) panneauRoot.getChildrenUnmodifiable().get(8);
		this.identifiant = (TextField) panneauRoot.getChildrenUnmodifiable().get(9);
		this.mdp = (TextField) panneauRoot.getChildrenUnmodifiable().get(10);
		this.soumettreRepresentant = (Button) panneauRoot.getChildrenUnmodifiable().get(11);
		this.nom.setText(representant.getNom());
		this.prenom.setText(representant.getPrenom());
		this.identifiant.setText(representant.getLogin());
		this.mdp.setText(representant.getPassword());
		this.soumettreRepresentant.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				envoyerModification(representant);
			}
		});;
	}

	protected void envoyerModification(Representant representant) {
		try {
			this.services.updateRepresentant(representant.getNumRepresentant(), this.nom.getText(), this.prenom.getText(), this.identifiant.getText(), this.mdp.getText());
			for (int i = 0; i < this.tableRepresentants.getItems().size(); i++) {
				Representant rep = this.tableRepresentants.getItems().get(i);
				if (rep.getNumRepresentant() == representant.getNumRepresentant()) {
					Representant newRepresentant = new Representant(this.nom.getText(), this.prenom.getText(), representant.getNumRepresentant(), this.identifiant.getText(), this.mdp.getText(), rep.getModifier(), rep.getSupprimer());
					this.tableRepresentants.getItems().set(i, newRepresentant);
				}
			}
			this.soumettreRepresentant.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					ajouterRepresentant();
				}
			});;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


