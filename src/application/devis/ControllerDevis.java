package application.devis;

import java.sql.SQLException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import com.sun.javafx.collections.ObservableListWrapper;

import Entity.Client;
import Entity.Devis;
import Entity.Ligne;
import Entity.Relance;
import Services.Services;
import application.Controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;

public class ControllerDevis extends Controller {

	// Page voir devis
	@FXML private Pane paneVoirDevis;
	@FXML private Label erreurDevis;
	@FXML private Label dateCreation;
	@FXML private Label dateFinValidite;
	@FXML private Label statut;
	@FXML private Label totalHt;
	@FXML private Label totalTva;
	@FXML private Label totalTtc;
	@FXML private ListView<Relance> listeRelances;
	@FXML private Button relance;
	@FXML private Button validation;
	@FXML private Button refus;
	@FXML private Button modifier;
	@FXML private TableView<Ligne> tabArticles;
	@FXML private TableColumn<Ligne, String> columnDesignation;
	@FXML private TableColumn<Ligne, Integer> columnQuantite;
	@FXML private TableColumn<Ligne, Double> columnPu;
	@FXML private TableColumn<Ligne, Double> columnTotalHt;
	@FXML private TableColumn<Ligne, Double> columnTotalTva;
	@FXML private TableColumn<Ligne, Double> columnTotalTtc;
	@FXML private TableColumn<Ligne, Button> columnSupprimer;
	@FXML private Label nomClient;
	@FXML private TextField nomArticle;
	@FXML private TextField prixArticle;
	@FXML private TextField quantiteArticle;
	@FXML private Label erreurSaisie;
	@FXML private Label erreurIntitule;
	@FXML private Label labelDesignation;
	@FXML private Label labelQuantite;
	@FXML private Label labelPu;
	@FXML private TableView<Ligne> tableVoirArticles;

	// Page liste devis
	@FXML private Pane paneDevis;
	@FXML private Button nouveauDevis;
	@FXML private TableView<Devis> listeDevis;
	@FXML private TableColumn<Devis, Integer> idDevis;
	@FXML private TableColumn<Devis, Date> dateDevis;
	@FXML private TableColumn<Devis, Date> dateFinDevis;
	@FXML private TableColumn<Devis, String> statutDevis;
	@FXML private TableColumn<Devis, String> client;
	@FXML private TableColumn<Devis, Double> total;
	@FXML private TableColumn<Devis, Button> colonneSuppr;

	// Page cr�er devis
	@FXML private Pane paneAjoutDevis;
	@FXML private TableView<Ligne> tableAjoutArticles;
	@FXML private Button enregistrerDevis;
	@FXML private DatePicker creationDateFin;
	@FXML private Button ajoutArticle;
	@FXML private ChoiceBox<Client> selectClient;
	@FXML private Label erreurDateFin;

	// Page modifier devis
	@FXML Button enregistrerModifDevis;

	boolean devisOk = true;
	private Label numDevis;

	public int numRepresentantCourant;



	public void creerDevis(int numRepresentant) {
		this.panneauPrincipal =  (BorderPane) this.paneDevis.getParent();
		chargerPage("ajouterDevis.fxml", this.panneauPrincipal);
		this.paneAjoutDevis = (Pane) this.panneauPrincipal.getCenter();
		this.selectClient = (ChoiceBox<Client>) ((HBox) this.paneAjoutDevis.getChildren().get(19)).getChildren().get(0);
		this.ajoutArticle = (Button) this.paneAjoutDevis.getChildren().get(15);
		this.tableAjoutArticles = (TableView<Ligne>) this.paneAjoutDevis.getChildren().get(3);
		this.ajoutArticle.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				ajouterArticle();
			}
		});
		// R�cup�ration des clients 
		Services s = new Services();
		ObservableList<Client> clients = FXCollections.observableArrayList();
		try {
			for (Client cli: s.getAllClients()) {
				clients.add(cli);
				this.selectClient.getItems().add(cli);
			}

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Initialisation des composants
		this.erreurDevis = (Label) this.paneAjoutDevis.getChildren().get(16);
		this.columnDesignation = (TableColumn<Ligne, String>) this.tableAjoutArticles.getColumns().get(0);
		this.columnQuantite = (TableColumn<Ligne, Integer>) this.tableAjoutArticles.getColumns().get(1);
		this.columnPu =  (TableColumn<Ligne, Double>) this.tableAjoutArticles.getColumns().get(2);
		this.columnTotalHt =  (TableColumn<Ligne, Double>) this.tableAjoutArticles.getColumns().get(3);
		this.columnTotalTva =  (TableColumn<Ligne, Double>) this.tableAjoutArticles.getColumns().get(4);
		this.columnTotalTtc =  (TableColumn<Ligne, Double>) this.tableAjoutArticles.getColumns().get(5);
		this.columnSupprimer = (TableColumn<Ligne, Button>) this.tableAjoutArticles.getColumns().get(6);
		this.columnDesignation.setCellValueFactory(new PropertyValueFactory<Ligne, String>("nomArticle"));
		this.columnQuantite.setCellValueFactory(new PropertyValueFactory<Ligne, Integer>("quantite"));
		this.columnPu.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("prixUnitaire"));
		this.columnTotalHt.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalHt"));
		this.columnTotalTva.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalTva"));
		this.columnTotalTtc.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalTtc"));
		this.columnSupprimer.setCellValueFactory(new PropertyValueFactory<Ligne, Button>("supprimer"));
		this.columnSupprimer.setStyle("-fx-alignment: CENTER;");
		this.nomArticle = (TextField) this.paneAjoutDevis.getChildren().get(20);
		this.quantiteArticle = (TextField) this.paneAjoutDevis.getChildren().get(21);
		this.prixArticle = (TextField) this.paneAjoutDevis.getChildren().get(22);
		this.erreurSaisie = (Label) this.paneAjoutDevis.getChildren().get(23);
		this.erreurIntitule = (Label) this.paneAjoutDevis.getChildren().get(24);
		this.totalHt = (Label) this.paneAjoutDevis.getChildren().get(10);
		this.totalTva = (Label) this.paneAjoutDevis.getChildren().get(11);
		this.totalTtc = (Label) this.paneAjoutDevis.getChildren().get(12);
		this.enregistrerDevis = (Button) this.paneAjoutDevis.getChildren().get(13);
		this.erreurDateFin = (Label) this.paneAjoutDevis.getChildren().get(18);
		this.creationDateFin = (DatePicker) this.paneAjoutDevis.getChildren().get(14);
		int num = this.numRepresentantCourant;
		this.enregistrerDevis.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				soumettreDevis(numRepresentant);
			}
		});
	}

	public void soumettreDevis(int numRepresentant) {
		this.devisOk = true;
		this.erreurDevis.setText("");
		checkDevis();
		if (this.tableAjoutArticles.getItems().size() == 0) {
			this.erreurDevis.setText("Veuillez renseigner au moins un article.");
		} else if (this.selectClient.getSelectionModel().getSelectedItem() == null) {
			this.erreurDevis.setText("Veuillez choisir un client.");
		}
		else {
			if (this.devisOk) {
				Date today = new Date();
				LocalDate localDate = this.creationDateFin.getValue();
				Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
				Date dateFin = Date.from(instant);

				Services s = new Services();

				try {
					int numDevis = s.addDevis(today, dateFin, 
							Double.parseDouble(this.totalHt.getText().substring(0, this.totalHt.getText().length()-1)), 
							Double.parseDouble(this.totalTva.getText().substring(0, this.totalTva.getText().length()-1)), 
							Double.parseDouble(this.totalTtc.getText().substring(0, this.totalTtc.getText().length()-1)), 
							0, this.selectClient.getSelectionModel().getSelectedItem().getNumClient(), numRepresentant);

					// Ajout des articles
					for (Ligne ligne:  this.tableAjoutArticles.getItems()) {
						s.addArticle(numDevis, ligne.getNomArticle(), ligne.getQuantite(), ligne.getPrixUnitaire(), ligne.getTotalHt(), ligne.getTotalTva(), ligne.getTotalTtc());
					}

				} catch (NumberFormatException | ClassNotFoundException | ParseException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				// Retour � la page principale
				chargerPage("listeDevis.fxml", (BorderPane) this.paneAjoutDevis.getParent());
				recupererDonneesDevis(panneauPrincipal, numRepresentant);
			}
		}
	}

	public void checkDevis() {
		try {
			LocalDate localDate = this.creationDateFin.getValue();
			Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			if (localDate.isBefore(LocalDate.now())) {
				this.erreurDateFin.setText("La date de fin ne peut pas �tre avant la date de cr�ation.");
				this.devisOk = false;
			}
		} catch (Exception e) {
			this.erreurDateFin.setText("Format incorrect.");
			this.devisOk = false;
		}
	}

	// Initialise le tableau listant tous les devis du repr�sentant connect�
	public void recupererDonneesDevis(BorderPane panneauPrincipal, int numRepresentant) {
		this.panneauPrincipal = panneauPrincipal;
		Services services = new Services();
		this.paneDevis = (Pane) this.panneauPrincipal.getCenter();
		this.listeDevis = (TableView<Devis>) this.paneDevis.getChildren().get(0);
		this.idDevis = (TableColumn<Devis, Integer>) this.listeDevis.getColumns().get(0);
		this.dateDevis = (TableColumn<Devis, Date>) this.listeDevis.getColumns().get(2);
		this.dateFinDevis = (TableColumn<Devis, Date>) this.listeDevis.getColumns().get(3);
		this.client = (TableColumn<Devis, String>) this.listeDevis.getColumns().get(1);
		this.total = (TableColumn<Devis, Double>) this.listeDevis.getColumns().get(4);
		this.statutDevis = (TableColumn<Devis, String>) this.listeDevis.getColumns().get(5);
		this.colonneSuppr = (TableColumn<Devis, Button>) this.listeDevis.getColumns().get(6);
		this.idDevis.setCellValueFactory(new PropertyValueFactory<Devis, Integer>("numDevis"));
		this.dateDevis.setCellValueFactory(new PropertyValueFactory<Devis, Date>("dateCreation"));
		this.statutDevis.setCellValueFactory(new PropertyValueFactory<Devis, String>("statut"));
		this.dateFinDevis.setCellValueFactory(new PropertyValueFactory<Devis, Date>("dateFinValidite"));
		this.total.setCellValueFactory(new PropertyValueFactory<Devis, Double>("totalTtc"));
		this.client.setCellValueFactory(new PropertyValueFactory<Devis, String>("nomClient"));
		this.colonneSuppr.setCellValueFactory(new PropertyValueFactory<Devis, Button>("supprimer"));
		this.nouveauDevis = (Button) this.paneDevis.getChildrenUnmodifiable().get(1);
		this.nouveauDevis.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				creerDevis(numRepresentant);
			}
		});
		ObservableList<Devis> data = FXCollections.observableArrayList();

		try {
			ArrayList<Client> clients = services.getAllClients();
			for (Devis devis: services.getAllDevis()) {
				// On ne r�cup�re que les devis du repr�sentant connect�
				if (devis.getNumRepresentant() == numRepresentant) {
					// R�cup�re le client pour pouvoir g�rer l'affichage dans le tableau de son nom/prenom
					Client client = new Client("", "", 0, "", "");
					for (Client cli: clients) {
						if (cli.getNumClient() == devis.getNumClient()) {
							client = cli;
						}
					}
					Image imgDelete = new Image("img/logo-delete.png");
					ImageView viewDelete = new ImageView(imgDelete);
					viewDelete.resize(15, 15);
					Button supprimer = new Button("", viewDelete);
					supprimer.setStyle("-fx-background-color:  DODGERBLUE;");
					supprimer.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent arg0) {
							supprimerDevis(devis);
						}
					});
					Devis dev = new Devis(devis.getNumDevis(), devis.getDateCreation(), devis.getDateFinValidite(), devis.getTotalHt(), devis.getTotalTva(), devis.getTotalTtc(), devis.getStatus(), devis.getNumClient(), client, numRepresentant, supprimer);
					data.add(dev);
				}
			}
			this.listeDevis.setItems(data);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void supprimerDevis(Devis devis) {
		Services s = new Services();
		try {
			for (Ligne l: s.getArticlesDevis(devis.getNumDevis())) {
				s.deleteArticle(l.getNumLigne());
			}
			for (Relance r: s.getAllRelances()) {
				if (r.getNumDevis() == devis.getNumDevis())
				s.deleteRelance(r.getNum());
			}
			s.deleteDevis(devis.getNumDevis());
			// Suppression des articles correspondants
			
			for (int i = 0; i<this.listeDevis.getItems().size(); i++) {
				if (this.listeDevis.getItems().get(i).getNumDevis() == devis.getNumDevis()) {
					this.listeDevis.getItems().remove(i);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void voirDevis() {
		Devis devis = this.listeDevis.getSelectionModel().getSelectedItem();
		try {
			this.panneauPrincipal = (BorderPane) this.paneDevis.getParent();
			chargerPage("voirDevis.fxml", this.panneauPrincipal);

			this.paneVoirDevis = (Pane) this.panneauPrincipal.getCenter();

			this.numDevis = (Label) this.paneVoirDevis.getChildren().get(1);
			this.dateCreation = (Label) this.paneVoirDevis.getChildren().get(6);
			this.dateFinValidite = (Label) this.paneVoirDevis.getChildren().get(7);
			this.statut = (Label) this.paneVoirDevis.getChildren().get(8);
			this.totalHt = (Label) this.paneVoirDevis.getChildren().get(15);
			this.totalTva = (Label) this.paneVoirDevis.getChildren().get(16);
			this.totalTtc = (Label) this.paneVoirDevis.getChildren().get(17);
			this.listeRelances = (ListView<Relance>) this.paneVoirDevis.getChildren().get(22);
			this.tableVoirArticles = (TableView<Ligne>) this.paneVoirDevis.getChildren().get(9);
			this.validation = (Button) this.paneVoirDevis.getChildren().get(23);
			this.refus = (Button) this.paneVoirDevis.getChildren().get(24);
			this.relance = (Button) this.paneVoirDevis.getChildren().get(19);
			this.modifier = (Button) this.paneVoirDevis.getChildren().get(18);
			this.nomClient = (Label) this.paneVoirDevis.getChildren().get(26);
			this.enregistrerModifDevis = (Button) this.paneVoirDevis.getChildren().get(28);
			this.ajoutArticle = (Button) this.paneVoirDevis.getChildren().get(29);
			this.ajoutArticle.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					ajouterArticle();
				}
			});
			this.creationDateFin = (DatePicker) this.paneVoirDevis.getChildren().get(27);
			this.numDevis.setText(Integer.toString(devis.getNumDevis()));
			
			// Initialisation du formulaire pour ajouter un article
			this.labelDesignation = (Label) this.paneVoirDevis.getChildren().get(31);
			this.labelQuantite = (Label) this.paneVoirDevis.getChildren().get(32);
			this.labelPu = (Label) this.paneVoirDevis.getChildren().get(33);
			
			// Initialisation de la liste des articles 
			this.tableAjoutArticles = (TableView<Ligne>) this.paneVoirDevis.getChildren().get(30);
			this.nomArticle = (TextField) this.paneVoirDevis.getChildren().get(34);
			this.quantiteArticle = (TextField) this.paneVoirDevis.getChildren().get(35);
			this.prixArticle = (TextField) this.paneVoirDevis.getChildren().get(36);
			this.columnDesignation = (TableColumn<Ligne, String>) this.tableVoirArticles.getColumns().get(0);
			this.columnQuantite = (TableColumn<Ligne, Integer>) this.tableVoirArticles.getColumns().get(1);
			this.columnPu =  (TableColumn<Ligne, Double>) this.tableVoirArticles.getColumns().get(2);
			this.columnTotalHt =  (TableColumn<Ligne, Double>) this.tableVoirArticles.getColumns().get(3);
			this.columnTotalTva =  (TableColumn<Ligne, Double>) this.tableVoirArticles.getColumns().get(4);
			this.columnTotalTtc =  (TableColumn<Ligne, Double>) this.tableVoirArticles.getColumns().get(5);
			this.columnDesignation.setCellValueFactory(new PropertyValueFactory<Ligne, String>("nomArticle"));
			this.columnQuantite.setCellValueFactory(new PropertyValueFactory<Ligne, Integer>("quantite"));
			this.columnPu.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("prixUnitaire"));
			this.columnTotalHt.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalHt"));
			this.columnTotalTva.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalTva"));
			this.columnTotalTtc.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalTtc"));
			
			// Initialisation des gestionnaires d'erreur
			this.erreurIntitule = (Label) this.paneVoirDevis.getChildren().get(37);
			this.erreurSaisie = (Label) this.paneVoirDevis.getChildren().get(38);

			
			Format formatter = new SimpleDateFormat("dd-MM-yyyy");
			Services services = new Services();
			
			// Initialisation des valeurs du devis dans les differents composants 
			
			// Affichage nom/pr�nom du client
			for (Client cli: services.getAllClients()) {
				if (cli.getNumClient() == devis.getNumClient()) {
					this.nomClient.setText(cli.getNom() + " " + cli.getPrenom());
				}
			}
			chargerDonneesArticles(devis.getNumDevis());			
			this.dateCreation.setText(formatter.format(devis.getDateCreation()));
			this.dateFinValidite.setText(formatter.format(devis.getDateFinValidite()));
			this.statut.setText(devis.getStatut());
			this.totalHt.setText(Double.toString(devis.getTotalHt()));
			this.totalTva.setText(Double.toString(devis.getTotalTva()));
			this.totalTtc.setText(Double.toString(devis.getTotalTtc()));
			
			String date = formatter.format(devis.getDateFinValidite());
			DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			this.creationDateFin.setValue(LocalDate.parse(date, formatter2));

			// �venements d�clanch�s par les boutons
			this.validation.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					changerStatut(devis, 1);
				}
			});
			this.refus.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					changerStatut(devis, 2);
				}
			});
			this.relance.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					relancer(devis);
				}
			});
			
			this.enregistrerModifDevis.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					envoyerModifDevis(devis);
				}
			});
			this.modifier.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					modifierDevis(devis.getNumDevis());
				}
			});

			// Certaines options sont d�sactiv�es selon le statut du devis
			if (devis.getStatus() != 0) {
				this.validation.setDisable(true);
				this.modifier.setDisable(true);
				this.refus.setDisable(true);
				this.relance.setDisable(true);
			}

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	// Initialise le tableau des articles avec les articles pr�sents en base
	private void chargerDonneesArticles(int numDevis) {
		Services s = new Services();
		ObservableList<Ligne> dataLire = FXCollections.observableArrayList();
		ObservableList<Ligne> dataAjouter = FXCollections.observableArrayList();
		try {

			for (Ligne l: s.getArticlesDevis(numDevis)) {
				dataLire.add(l);
				Image imgDelete = new Image("img/logo-delete.png");
				ImageView viewDelete = new ImageView(imgDelete);
				viewDelete.resize(15, 15);
				Button supprimer = new Button("", viewDelete);
				supprimer.setStyle("-fx-background-color:  DODGERBLUE;");
				supprimer.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent arg0) {
						supprimerArticle(l);
					}
				});
				dataAjouter.add(new Ligne(l.getNumLigne(), l.getNumDevis(), l.getNomArticle(), l.getQuantite(), l.getPrixUnitaire(), l.getTotalHt(), l.getTotalTva(), l.getTotalTtc(), supprimer));
			}
			this.tableVoirArticles.setItems(dataLire);
			this.tableAjoutArticles.setItems(dataAjouter);

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Gestion de l'affichage suite � une insertion d'un article dans le tableau
	public void ajouterArticle() {
		this.erreurSaisie.setText("");
		this.erreurIntitule.setText("");
		if (this.nomArticle.getText().length() == 0) {
			this.erreurIntitule.setText("La d�signation est obligatoire");
		}
		else {
			try {
				Double prixHt = Double.parseDouble(this.quantiteArticle.getText()) * Double.parseDouble(this.prixArticle.getText());
				Double prixTva = prixHt / 100 * 20;
				Double prixTtc = prixHt + prixTva;
				Image imgDelete = new Image("img/logo-delete.png");
				ImageView viewDelete = new ImageView(imgDelete);
				viewDelete.resize(15, 15);
				Button supprimer = new Button("", viewDelete);
				supprimer.setStyle("-fx-background-color:  DODGERBLUE;");
				Ligne ligne = new Ligne(-1, 0, this.nomArticle.getText(), Integer.parseInt(this.quantiteArticle.getText()), Double.parseDouble(this.prixArticle.getText()), prixHt, prixTva, prixTtc, supprimer);
				supprimer.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent arg0) {
						supprimerArticle(ligne);
					}
				});
				this.tableAjoutArticles.getItems().add(ligne);

				Double totHt;
				Double totTva;
				Double totTtc;

				if (this.totalHt.getText().length() == 0) {
					totHt = (double) 0;
				} else {
					totHt = Double.parseDouble(this.totalHt.getText().substring(0, this.totalHt.getText().length()-1));
				}
				totHt += prixHt;
				if (this.totalTva.getText().length() == 0) {
					totTva = (double) 0;
				} else {
					totTva = Double.parseDouble(this.totalTva.getText().substring(0, this.totalTva.getText().length()-1));
				}
				totTva += prixTva;
				if (this.totalTtc.getText().length() == 0) {
					totTtc = (double) 0;
				} else {
					totTtc = Double.parseDouble(this.totalTtc.getText().substring(0, this.totalTtc.getText().length()-1));
				}
				totTtc += prixTtc;
				this.totalHt.setText(Double.toString(Math.round(totHt* 100.0)/100.0) + "�");
				this.totalTva.setText(Double.toString(Math.round(totTva* 100.0)/100.0) + "�");
				this.totalTtc.setText(Double.toString(Math.round(totTtc* 100.0)/100.0) + "�");
				
				viderFormulaire();
			} catch (NumberFormatException e) {
				this.erreurSaisie.setText("Les donn�es saisies sont incorrectes.");
			}

		} 
	}
	
	public void viderFormulaire() {
		this.nomArticle.setText("");
		this.quantiteArticle.setText("");
		this.prixArticle.setText("");
	}

	// Gestion de l'affichage suite � une suppression d'un article dans le tableau
	private void supprimerArticle(Ligne ligne) {
		for (int i = 0; i < this.tableAjoutArticles.getItems().size(); i++) {
			Ligne lig = this.tableAjoutArticles.getItems().get(i);
			if (lig.getNumLigne() == ligne.getNumLigne()){
				this.tableAjoutArticles.getItems().remove(i);
				this.totalHt.setText(Double.toString(Double.parseDouble(this.totalHt.getText().substring(0, this.totalHt.getText().length()-1)) - lig.getTotalHt()));
				this.totalTva.setText(Double.toString(Double.parseDouble(this.totalTva.getText().substring(0, this.totalTva.getText().length()-1)) - lig.getTotalTva()));
				this.totalTtc.setText(Double.toString(Double.parseDouble(this.totalTtc.getText().substring(0, this.totalTtc.getText().length()-1)) - lig.getTotalTtc()));
			}
		}
	}

	// Gestion des relances
	public void relancer(Devis d) {
		Services s = new Services();
		int count = 0;
		try {
			for (Relance relance: s.getAllRelances()) {
				if (relance.getNumDevis() == d.getNumDevis()) {
					count ++;
				}
			}
			this.listeRelances.getItems().add(s.addRelance(d.getNumDevis(), new Date()));
			if (count == 2) {
				changerStatut(d, 2);
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Modification du statut du devis suite � une relance - Gestion de l'affichage dynamique
	public void changerStatut(Devis d, int statut) {
		Services services = new Services();
		try {
			services.changerStatutDevis(statut, d.getNumDevis());
			this.validation.setDisable(true);
			this.modifier.setDisable(true);
			this.refus.setDisable(true);
			this.relance.setDisable(true);
			if (statut == 1) {
				this.statut.setText("Command�");
			} else if (statut == 2) {
				this.statut.setText("Obsol�te");
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Soumission du formulaire de modification du devis
	public void envoyerModifDevis(Devis d) {

		this.enregistrerModifDevis.setVisible(false);
		this.enregistrerModifDevis.setDisable(true);
		this.ajoutArticle.setVisible(false);
		this.ajoutArticle.setDisable(true);
		this.creationDateFin.setVisible(false);
		this.creationDateFin.setDisable(true);
		this.tableAjoutArticles.setVisible(false);
		this.labelDesignation.setVisible(true);
		this.labelQuantite.setVisible(false);
		this.labelPu.setVisible(false);
		this.quantiteArticle.setVisible(false);
		this.nomArticle.setVisible(false);
		this.prixArticle.setVisible(false);

		this.modifier.setVisible(true);
		this.tableVoirArticles.setVisible(true);

		LocalDate localDate = this.creationDateFin.getValue();
		Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
		Date dateFin = Date.from(instant);

		Services s = new Services();

		try {
			s.updateDevis(d.getNumDevis(), 
					d.getDateCreation(), 
					dateFin, 
					Double.parseDouble(this.totalHt.getText().substring(0, this.totalHt.getText().length() -1)), 
					Double.parseDouble(this.totalTva.getText().substring(0, this.totalTva.getText().length()-1)), 
					Double.parseDouble(this.totalTtc.getText().substring(0, this.totalTtc.getText().length()-1)), 
					d.getStatus(), 
					d.getNumClient(), 
					d.getNumRepresentant());

			traiterLignesAModifier(d.getNumDevis());

		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		chargerPage("listeDevis.fxml", (BorderPane) this.panneauPrincipal);
		recupererDonneesDevis(panneauPrincipal, d.getNumRepresentant());
	}


	// G�rer les �ventuels ajouts/suppressions d'articles dans la modification du devis
	private void traiterLignesAModifier(int numDevis) {
		Services s = new Services();
		try {
			ArrayList<Ligne> lignes = s.getArticlesDevis(numDevis);
			for (Ligne ligne: this.tableAjoutArticles.getItems()) {
				if (ligne.getNumLigne() == -1) {
					s.addArticle(numDevis, ligne.getNomArticle(), ligne.getQuantite(), ligne.getPrixUnitaire(), ligne.getTotalHt(), ligne.getTotalTva(), ligne.getTotalTtc());
				}}
			for (Ligne lig: lignes) {
				int count = 0;
				for (Ligne ligTab: this.tableAjoutArticles.getItems()) {
					if (ligTab.getNumLigne() == lig.getNumLigne()) {
						count ++;
					}
				}
				if (count == 0) {
					s.deleteArticle(lig.getNumLigne());
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Gestion de l'affichage pour la modification du devis
	public void modifierDevis(int numDevis) {
		this.enregistrerModifDevis.setVisible(true);
		this.enregistrerModifDevis.setDisable(false);
		this.ajoutArticle.setVisible(true);
		this.ajoutArticle.setDisable(false);
		this.creationDateFin.setVisible(true);
		this.creationDateFin.setDisable(false);
		this.tableAjoutArticles.setVisible(true);
		this.labelDesignation.setVisible(true);
		this.labelQuantite.setVisible(true);
		this.labelPu.setVisible(true);
		this.quantiteArticle.setVisible(true);
		this.nomArticle.setVisible(true);
		this.prixArticle.setVisible(true);

		this.ajoutArticle.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				ajouterArticle();
			}
		});

		this.columnDesignation = (TableColumn<Ligne, String>) this.tableAjoutArticles.getColumns().get(0);
		this.columnQuantite = (TableColumn<Ligne, Integer>) this.tableAjoutArticles.getColumns().get(1);
		this.columnPu = (TableColumn<Ligne, Double>) this.tableAjoutArticles.getColumns().get(2);
		this.columnTotalHt = (TableColumn<Ligne, Double>) this.tableAjoutArticles.getColumns().get(3);
		this.columnTotalTva = (TableColumn<Ligne, Double>) this.tableAjoutArticles.getColumns().get(4);
		this.columnTotalTtc = (TableColumn<Ligne, Double>) this.tableAjoutArticles.getColumns().get(5);
		this.columnSupprimer = (TableColumn<Ligne, Button>) this.tableAjoutArticles.getColumns().get(6);
		this.columnDesignation.setCellValueFactory(new PropertyValueFactory<Ligne, String>("nomArticle"));
		this.columnQuantite.setCellValueFactory(new PropertyValueFactory<Ligne, Integer>("quantite"));
		this.columnPu.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("prixUnitaire"));
		this.columnTotalHt.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalHt"));
		this.columnTotalTva.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalTva"));
		this.columnTotalTtc.setCellValueFactory(new PropertyValueFactory<Ligne, Double>("totalTtc"));
		this.columnSupprimer.setCellValueFactory(new PropertyValueFactory<Ligne, Button>("supprimer"));

		this.modifier.setVisible(false);
		this.tableVoirArticles.setVisible(false);

		chargerDonneesArticles(numDevis);

	}
}

