package application;
	
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import Entity.Client;
import Entity.Devis;
import Entity.Ligne;
import Entity.Personne;
import Entity.Relance;
import Entity.Representant;
import Modele.ClientBDD;
import Modele.ConnectBDD;
import Modele.DevisBDD;
import Modele.LigneBDD;
import Modele.RelanceBDD;
import Modele.RepresentantBDD;
import Services.Services;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class MainTest extends Application {
	@Override
	public void start(Stage primaryStage)  {
			
		try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, ParseException {
		
		Date d1 = new Date();
		
		Calendar date_creation = Calendar.getInstance();
		DateFormat format = new SimpleDateFormat("hh:mm:ss"); 
		Date now = format.parse("01:01:01"); 
		date_creation.setTime(d1);

		Calendar date_fin_validite = Calendar.getInstance();
		date_fin_validite.setTime(now);
		date_fin_validite.set(Calendar.YEAR, 2020);
		date_fin_validite.set(Calendar.DATE, 24);
		date_fin_validite.set(Calendar.MONTH, 10);
		
		//Devis devis1 = new Devis(2,date_creation.getTime(),date_fin_validite.getTime(),10.0,11.2,14.5,0);
		DevisBDD devisBDD = new DevisBDD();
		//devisBDD.create_devis(devis1);
		Services serv = new Services();

		try {
			serv.getId("client");
		}catch(NumberFormatException e) {
			System.out.println("null");
		}

	}
}
