package Services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Entity.Client;
import Entity.Devis;
import Entity.Ligne;
import Entity.Relance;
import Entity.Representant;
import Modele.AutoIncrement;
import Modele.ClientBDD;
import Modele.DevisBDD;
import Modele.LigneBDD;
import Modele.RelanceBDD;
import Modele.RepresentantBDD;

public class Services {

	private ClientBDD cliBDD = new ClientBDD();
	private DevisBDD devBDD = new DevisBDD();
	private LigneBDD lineBDD = new LigneBDD();
	private RelanceBDD relBDD = new RelanceBDD();
	private RepresentantBDD repBDD = new RepresentantBDD();
	private AutoIncrement autoID = new AutoIncrement();
	
	public Services() {
	}
	// GET
	public ArrayList<Client> getAllClients() throws ClassNotFoundException, SQLException {
		return cliBDD.getAllClients();
	}
	
	public ArrayList<Devis>getAllDevis() throws ClassNotFoundException, SQLException {
		return devBDD.getAllDevis();
	}
	
	public ArrayList<Relance>getAllRelances() throws ClassNotFoundException, SQLException {
		return relBDD.getAllRelances();
	}
	
	
	public ArrayList<Representant>getAllRepresentants() throws ClassNotFoundException, SQLException {
		return repBDD.getAllRepresentants();
	}
	
	public ArrayList<Ligne> getArticlesDevis(int num_devis) throws ClassNotFoundException, SQLException {
		return lineBDD.getAllLignesDevis(num_devis);
	}
	// ADD
	public void addClient(String nom , String prenom, String raison_sociale, String adresse_postale) throws ClassNotFoundException, SQLException {
		Client cli = new Client(nom,prenom, this.getId("client"), raison_sociale, adresse_postale);
		cliBDD.createClient(cli);
	}
	
	public int addDevis(Date date_creation, Date date_fin_validite, double total_ht, double total_tva, double total_ttc, int status, int num_client, int num_representant) throws ParseException, ClassNotFoundException, SQLException {
		int id = this.getId("devis");
		Calendar Cdate_creation = Calendar.getInstance(); 
		Cdate_creation.setTime(date_creation);
		Calendar Cdate_fin_validite = Calendar.getInstance();
		Cdate_fin_validite.setTime(date_fin_validite);
		
		Devis devis = new Devis(id,Cdate_creation.getTime(),Cdate_fin_validite.getTime(),total_ht,total_tva,total_ttc,status, num_client, num_representant);
		devBDD.createDevis(devis);
		return id;
	}
	
	public void addArticle(int num_devis, String nom_article, int quantite, double prix_unitaire , double total_ht, double total_tva, double total_ttc) throws ClassNotFoundException, SQLException {
		Ligne article = new Ligne(this.getId("ligne"),num_devis,nom_article,quantite,prix_unitaire,total_ht,total_tva,total_ttc);
		lineBDD.createLigne(article);
	}
	
	public Relance addRelance(int num_devis, Date date_relance) throws ClassNotFoundException, SQLException {
		
		Calendar Cdate_relance = Calendar.getInstance(); 
		Cdate_relance.setTime(date_relance);
		
		Relance relance = new Relance(this.getId("relance"),num_devis,Cdate_relance.getTime());
		relBDD.createRelance(relance);
		return relance;
	}
	
	public int addRepresentant(String nom, String prenom, String login, String password) throws ClassNotFoundException, SQLException {
		int id = this.getId("representant");
		Representant rep = new Representant(nom,prenom,id,login,password);
		repBDD.createRepresentant(rep);
		return id;
	}
	//DELETE
	public void deleteClient(int num_client) throws ClassNotFoundException, SQLException {
		cliBDD.deleteClient(num_client);
	}
	
	public void deleteDevis(int num_devis) throws ClassNotFoundException, SQLException {
		devBDD.supprDevis(num_devis);
	}
	
	public void deleteArticle(int num_article) throws ClassNotFoundException, SQLException {
		lineBDD.deleteLigne(num_article);
	}
	
	public void deleteRepresentant(int num_rep) throws ClassNotFoundException, SQLException {
		repBDD.deleteRepresentant(num_rep);
	}
	
	//UPDATE
	public void updateClient(int num_client, String nom, String prenom,String raison_sociale, String adresse_postale) throws ClassNotFoundException, SQLException {
		Client cli = new Client(nom,prenom, num_client, raison_sociale, adresse_postale);
		cliBDD.modifyClient(cli);
	}
	
	public void updateRepresentant(int num, String nom, String prenom,String login, String password) throws ClassNotFoundException, SQLException {
		Representant rep = new Representant(nom, prenom, num, login, password);
		repBDD.modifyRepresentant(rep);
	}
	
	public void updateDevis(int num_devis, Date date_creation, Date date_fin_validite, double total_ht, double total_tva, double total_ttc, int status, int num_client, int num_representant) throws ClassNotFoundException, SQLException {
		Calendar Cdate_creation = Calendar.getInstance(); 
		Cdate_creation.setTime(date_creation);
		Calendar Cdate_fin_validite = Calendar.getInstance();
		Cdate_fin_validite.setTime(date_fin_validite);
		
		Devis devis = new Devis(num_devis,Cdate_creation.getTime(),Cdate_fin_validite.getTime(),total_ht,total_tva,total_ttc,status,num_client,num_representant);
		devBDD.modifyDevis(devis);	
	}
	
	public void updateArticle(int num_ligne, int num_devis, String nom_article, int quantite, double prix_unitaire , double total_ht, double total_tva, double total_ttc) throws ClassNotFoundException, SQLException {
		Ligne article = new Ligne(num_ligne,num_devis,nom_article,quantite,prix_unitaire,total_ht,total_tva,total_ttc);
		lineBDD.modifyLigne(article);
	}
	
	// O = refuser | 1 = valider
	public void changerStatutDevis(int status, int num_devis) throws ClassNotFoundException, SQLException {
		devBDD.modifyStatusDevis(status, num_devis);
	}
	
	//Auto-Increment
	public Integer getId(String table) throws SQLException, ClassNotFoundException {
		try {
			return autoID.getId(table);
		}catch(NumberFormatException e) {
			return 1;
		}
	}
	public void deleteRelance(int num) {
		try {
			relBDD.deleteRelance(num);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
