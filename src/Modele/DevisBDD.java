package Modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import Entity.Devis;
import Entity.Ligne;

public class DevisBDD {

	private ConnectBDD cn = new ConnectBDD();
	
	private ArrayList<Devis> all_devis = new ArrayList <Devis> ();
	
	public DevisBDD() {
	}
	
	public void createDevis(Devis dev) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "INSERT INTO devis (num, date_creation, date_fin_validite, total_ht, total_tva, total_ttc, status, num_client, num_representant) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement updateQuery = Connect.prepareStatement(sql);
		java.sql.Date SqlDate_creation = convert(dev.getDateCreation());
		java.sql.Date SqlDate_fin_validite = convert(dev.getDateFinValidite());
		
		updateQuery.setInt(1, dev.getNumDevis());
		updateQuery.setDate(2, SqlDate_creation);
		updateQuery.setDate(3, SqlDate_fin_validite);
		updateQuery.setDouble(4, dev.getTotalHt());
		updateQuery.setDouble(5, dev.getTotalTva());
		updateQuery.setDouble(6, dev.getTotalTtc());
		updateQuery.setInt(7, dev.getStatus());
		updateQuery.setInt(8, dev.getNumClient());
		updateQuery.setInt(9, dev.getNumRepresentant());
		try{
			updateQuery.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		cn.DeconnectedBDD(Connect, updateQuery);
	}
	
	public Devis getDevisId(int id) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		ResultSet rs;
		String sql = "SELECT * FROM devis where num = ?";
		PreparedStatement SelectQuery = Connect.prepareStatement(sql);
		SelectQuery.setInt(1, id);
		rs = SelectQuery.executeQuery();
		
		Devis devis = new Devis(0,new Date(),new Date(),0.0,0.0,0.0,0,0,0);
		while (rs.next()) {
			devis.setNumDevis(rs.getInt("num"));
			devis.setDateCreation(rs.getDate("date_creation"));
			devis.setDateFinValidite(rs.getDate("date_fin_validite"));
			devis.setTotalHt(rs.getDouble("total_ht"));
			devis.setTotalTva(rs.getDouble("total_tva"));
			devis.setTotalTtc(rs.getDouble("total_ttc"));
			devis.setStatus(rs.getInt("status"));
			devis.setNumClient(rs.getInt("num_client"));
			devis.setNumRepresentant(rs.getInt("num_representant"));
		}
		cn.DeconnectedBDD(Connect, SelectQuery);
		return devis;
	}
	
	public ArrayList<Devis> getAllDevis() throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		ResultSet rs;
		String sql = "SELECT * FROM Devis";
		PreparedStatement SelectQuery = Connect.prepareStatement(sql);
		rs = SelectQuery.executeQuery();
		
		while (rs.next()) {
			Devis dev = new Devis(rs.getInt("num"),rs.getDate("date_creation"),rs.getDate("date_fin_validite"),rs.getDouble("total_ht"),rs.getDouble("total_tva"),rs.getDouble("total_ttc"),rs.getInt("status"),rs.getInt("num_client"),rs.getInt("num_representant"));
			all_devis.add(dev);
		}
		cn.DeconnectedBDD(Connect, SelectQuery);
		return all_devis;
	}
	
	public Devis modifyDevis(Devis dev) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "UPDATE DEVIS SET date_creation = ?, date_fin_validite = ?, total_ht = ?, total_tva = ?, total_ttc = ?, status = ? WHERE num= ? ";
		PreparedStatement UpdateQuery = Connect.prepareStatement(sql);
		UpdateQuery.setDate(1, convert(dev.getDateCreation()));
		UpdateQuery.setDate(2, convert(dev.getDateFinValidite()));
		UpdateQuery.setDouble(3, dev.getTotalHt());
		UpdateQuery.setDouble(4, dev.getTotalTva());
		UpdateQuery.setDouble(5, dev.getTotalTtc());
		UpdateQuery.setDouble(6, dev.getStatus());
		UpdateQuery.setInt(7, dev.getNumDevis());
		
		try{
			UpdateQuery.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		cn.DeconnectedBDD(Connect, UpdateQuery);
		return this.getDevisId(dev.getNumDevis());
	}
	
	public void modifyStatusDevis(int status,int id_devis) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "UPDATE DEVIS SET status = ? WHERE num= ? ";
		PreparedStatement UpdateQuery = Connect.prepareStatement(sql);
		UpdateQuery.setInt(1, status);
		UpdateQuery.setInt(2, id_devis);

		try{
			UpdateQuery.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		cn.DeconnectedBDD(Connect, UpdateQuery);
	}
	
	public void supprDevis(int id) throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "DELETE FROM DEVIS WHERE num = ?";
		PreparedStatement DeleteQuery = Connect.prepareStatement(sql);
		DeleteQuery.setInt(1, id);
		DeleteQuery.executeUpdate();
		cn.DeconnectedBDD(Connect, DeleteQuery);
	}
	
	private static java.sql.Date convert(Date Udate) {
        java.sql.Date sDate = new java.sql.Date(Udate.getTime());
        return sDate;
	}
	
	
}
