package Modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AutoIncrement {

	private ConnectBDD cn = new ConnectBDD();
	
	public AutoIncrement() {
		
	}
	
	public Integer getId(String table) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		ResultSet rs;
		String id = null ;
		String sql = "SELECT Max(num) FROM "+table;
		PreparedStatement SelectQuery = Connect.prepareStatement(sql);
		rs = SelectQuery.executeQuery();
		
		while (rs.next()) {			
			id  = rs.getString(1);
		}
		
		cn.DeconnectedBDD(Connect, SelectQuery);
		return Integer.parseInt(id)+1;
	}
}
