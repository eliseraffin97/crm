package Modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Entity.Client;
import Entity.Ligne;
import Entity.Personne;
import Entity.Relance;

public class ClientBDD {

	private ConnectBDD cn = new ConnectBDD();

	private ArrayList<Client> allClients = new ArrayList <Client> ();

	public ClientBDD() {
	}

	public void createClient(Client cli) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "INSERT INTO client (num, nom, prenom, raison_sociale, addresse_postale) VALUES (?, ?, ?, ?, ?)";
		PreparedStatement CreateQuery = Connect.prepareStatement(sql);

		CreateQuery.setInt(1, cli.getNumClient());
		CreateQuery.setString(2, cli.getNom());
		CreateQuery.setString(3, cli.getPrenom());
		CreateQuery.setString(4, cli.getRaisonSociale());
		CreateQuery.setString(5, cli.getAdressePostale());

		try{
			CreateQuery.executeUpdate();
		}catch(SQLException e) {
		}
		cn.DeconnectedBDD(Connect, CreateQuery);
	}

	public void deleteClient(int numClient) throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "DELETE FROM client where num = ?";
		PreparedStatement DeleteQuery = Connect.prepareStatement(sql);	
		DeleteQuery.setInt(1, numClient);
		try{
			DeleteQuery.executeUpdate();
		}catch(SQLException e) {
		}
		cn.DeconnectedBDD(Connect, DeleteQuery);
	}

	public void modifyClient(Client cli) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "UPDATE client SET nom = ?, prenom = ?, raison_sociale = ?, addresse_postale = ? WHERE num= ?";
		PreparedStatement UpdateQuery = Connect.prepareStatement(sql);
		UpdateQuery.setString(1, cli.getNom());
		UpdateQuery.setString(2, cli.getPrenom());
		UpdateQuery.setString(3, cli.getRaisonSociale());
		UpdateQuery.setString(4, cli.getAdressePostale());
		UpdateQuery.setInt(5, cli.getNumClient());

		try  {
			UpdateQuery.executeUpdate();
		}catch(SQLException e) {
		}
		cn.DeconnectedBDD(Connect, UpdateQuery);
	}

	public ArrayList<Client> getAllClients() throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		ResultSet rs;
		String sql = "SELECT * FROM Client";
		PreparedStatement SelectQuery = Connect.prepareStatement(sql);
		rs = SelectQuery.executeQuery();

		while (rs.next()) {
			Client cli = new Client(rs.getString("nom"),rs.getString("prenom"),rs.getInt("num"),rs.getString("raison_sociale"),rs.getString("addresse_postale"));
			allClients.add(cli);
		}
		cn.DeconnectedBDD(Connect, SelectQuery);
		return allClients;
	}


}
