package Modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import Entity.Client;
import Entity.Devis;
import Entity.Relance;

public class RelanceBDD {

	private ConnectBDD cn = new ConnectBDD();
	
	public RelanceBDD() {
	}
	
	public void createRelance(Relance relance) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "INSERT INTO relance (num, num_devis, date_relance) VALUES (?, ?, ?)";
		PreparedStatement CreateQuery = Connect.prepareStatement(sql);
		java.sql.Date SqlDate_creation = convert(relance.getDateRelance());
		
		CreateQuery.setInt(1, relance.getNum());
		CreateQuery.setInt(2, relance.getNumDevis());
		CreateQuery.setDate(3, SqlDate_creation);

		try{
			CreateQuery.executeUpdate();
		}catch(SQLException e) {
			System.out.println("d�j� existant");
		}
		cn.DeconnectedBDD(Connect, CreateQuery);
	}
	
	public ArrayList<Relance> getAllRelances() throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		ResultSet rs;
		String sql = "SELECT * FROM Relance";
		PreparedStatement SelectQuery = Connect.prepareStatement(sql);
		rs = SelectQuery.executeQuery();
		
		ArrayList<Relance> all_relances = new ArrayList<Relance>();
		while (rs.next()) {
			Relance rel = new Relance(rs.getInt("num"), rs.getInt("num_devis"), rs.getDate("date_relance"));
			all_relances.add(rel);
		}
		cn.DeconnectedBDD(Connect, SelectQuery);
		return all_relances;
	}
	
	private static java.sql.Date convert(Date Udate) {
        java.sql.Date sDate = new java.sql.Date(Udate.getTime());
        return sDate;
	}

	public void deleteRelance(int num)  throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "DELETE FROM relance where num = ?";
		PreparedStatement DeleteQuery = Connect.prepareStatement(sql);	
		DeleteQuery.setInt(1, num);
		try{
			DeleteQuery.executeUpdate();
		}catch(SQLException e) {
		}
		cn.DeconnectedBDD(Connect, DeleteQuery);
		
	}

}
