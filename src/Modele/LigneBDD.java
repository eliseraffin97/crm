package Modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import Entity.Devis;
import Entity.Ligne;

public class LigneBDD {

private ConnectBDD cn = new ConnectBDD();
	
	private ArrayList<Ligne> lignesDevis = new ArrayList <Ligne> ();

	public LigneBDD() {
	}
	
	public void createLigne(Ligne line) throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "INSERT INTO ligne (num, num_devis, nom_article, quantite, prix_unitaire, total_ht, total_tva, total_ttc) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement CreateQuery = Connect.prepareStatement(sql);
		CreateQuery.setInt(1, line.getNumLigne());
		CreateQuery.setInt(2, line.getNumDevis());
		CreateQuery.setString(3, line.getNomArticle());
		CreateQuery.setInt(4, line.getQuantite());
		CreateQuery.setDouble(5, line.getPrixUnitaire());
		CreateQuery.setDouble(6, line.getTotalHt());
		CreateQuery.setDouble(7, line.getTotalTva());
		CreateQuery.setDouble(8, line.getTotalTtc());
		try{
			CreateQuery.executeUpdate();
		}catch(SQLException e) {
		}
		cn.DeconnectedBDD(Connect, CreateQuery);
	}
	
	public ArrayList<Ligne> getAllLignesDevis(int id) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		ResultSet rs;
		String sql = "SELECT * FROM ligne where num_devis = ?";
		PreparedStatement SelectQuery = Connect.prepareStatement(sql);
		SelectQuery.setInt(1, id);
		rs = SelectQuery.executeQuery();
		
		while (rs.next()) {
			Ligne line = new Ligne(rs.getInt("num"),rs.getInt("num_devis"),rs.getString("nom_article"),rs.getInt("quantite"),rs.getDouble("prix_unitaire"),rs.getDouble("total_ht"),rs.getDouble("total_tva"),rs.getDouble("total_ttc"));
			lignesDevis.add(line);
		}
		cn.DeconnectedBDD(Connect, SelectQuery);
		return lignesDevis;
	}
	
	public void deleteLigne(int id_line) throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "DELETE FROM ligne where num = ?";
		PreparedStatement DeleteQuery = Connect.prepareStatement(sql);	
		DeleteQuery.setInt(1, id_line);
		try{
			DeleteQuery.executeUpdate();
		}catch(SQLException e) {
		}
		cn.DeconnectedBDD(Connect, DeleteQuery);
	}
	
	public void modifyLigne(Ligne line) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "UPDATE LIGNE SET num_devis = ?, nom_article = ?, quantite = ?, prix_unitaire = ?, total_ht = ?, total_tva = ?, total_ttc = ? WHERE num= ? ";
		PreparedStatement UpdateQuery = Connect.prepareStatement(sql);
		UpdateQuery.setInt(1, line.getNumDevis());
		UpdateQuery.setString(2, line.getNomArticle());
		UpdateQuery.setInt(3, line.getQuantite());
		UpdateQuery.setDouble(4, line.getPrixUnitaire());
		UpdateQuery.setDouble(5, line.getTotalHt());
		UpdateQuery.setDouble(6, line.getTotalTva());
		UpdateQuery.setDouble(7, line.getTotalTtc());
		try  {
			UpdateQuery.executeUpdate();
		}catch(SQLException e) {
		}
		
		cn.DeconnectedBDD(Connect, UpdateQuery);
	}
}

