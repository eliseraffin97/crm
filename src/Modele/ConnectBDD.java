package Modele;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

	public class ConnectBDD {
		
		private String url = "jdbc:derby:C:/Users/ELeve/eclipse-workspace/crm/quantumDB;create=true";
		private String login = "QUANTUM";
		private String passwd = "";
		private Connection cn = null;	
		
		public ConnectBDD() {
			// SELECT
			/**st = cn.createStatement();
			String sql = "SELECT * FROM utilisateur";
			rs = st.executeQuery(sql);
			while (rs.next()) {
				System.out.println(rs.getString("nom"));
			}**/
			
			// INSERT
			/**st = cn.createStatement();
			String sql = "INSERT INTO utilisateur VALUES (2,'Costes','Quentin','Qcostes95@gmail.com','1999-05-14','France','Toulouse','31100',9)";
			st.executeUpdate(sql);**/
			
			// UPDATE | DELETE
			/**String raison = "IMS";
			String sql = "UPDATE CLIENT SET raison_sociale = ? WHERE num=1 ";
			PreparedStatement updateQuery = cn.prepareStatement(sql);
			updateQuery.setString(1, raison);
			updateQuery.executeUpdate();**/
			
			//this.DeconnectBDD(cn,updateQuery);
		}
		public Connection ConnectedBDD() throws ClassNotFoundException, SQLException {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			cn = DriverManager.getConnection(url,login,passwd);
			System.out.println("Connection � la base de donn�es");
			return cn;
		}
		public void DeconnectedBDD(Connection Cn,Statement st) {
			try {
				Cn.commit();
				Cn.close();
				st.close();
				DriverManager.getConnection (url+"; shutdown = true");
			}catch (SQLException e){
				System.out.println("Fin requ�te");
			}
		}
		
}	
