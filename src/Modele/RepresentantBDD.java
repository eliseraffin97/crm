package Modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Entity.Client;
import Entity.Devis;
import Entity.Ligne;
import Entity.Representant;

public class RepresentantBDD {

	private ConnectBDD cn = new ConnectBDD();
	
	private ArrayList<Representant> all_representants = new ArrayList <Representant> ();
	
	public RepresentantBDD() {
	}
	
	public void createRepresentant(Representant rep) throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "INSERT INTO representant (num, nom, prenom, login, password) VALUES (?, ?, ?, ?, ?)";
		PreparedStatement CreateQuery = Connect.prepareStatement(sql);
		CreateQuery.setInt(1, rep.getNumRepresentant());
		CreateQuery.setString(2, rep.getNom());
		CreateQuery.setString(3, rep.getPrenom());
		CreateQuery.setString(4, rep.getLogin());
		CreateQuery.setString(5, rep.getPassword());

		try{
			CreateQuery.executeUpdate();
		}catch(SQLException e) {
			System.out.println("D�j� existant");
		}
		cn.DeconnectedBDD(Connect, CreateQuery);
	}
	
	public void deleteRepresentant(int num_rep) throws ClassNotFoundException, SQLException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "DELETE FROM representant where num = ?";
		PreparedStatement DeleteQuery = Connect.prepareStatement(sql);	
		DeleteQuery.setInt(1, num_rep);
		try{
			DeleteQuery.executeUpdate();
		}catch(SQLException e) {
		}
		cn.DeconnectedBDD(Connect, DeleteQuery);
	}
	
	public void modifyRepresentant(Representant rep) throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		String sql = "UPDATE representant SET nom = ?, prenom = ?, login = ?, password = ? WHERE num= ?";
		PreparedStatement UpdateQuery = Connect.prepareStatement(sql);
		UpdateQuery.setString(1, rep.getNom());
		UpdateQuery.setString(2, rep.getPrenom());
		UpdateQuery.setString(3, rep.getLogin());
		UpdateQuery.setString(4, rep.getPassword());
		UpdateQuery.setInt(5, rep.getNumRepresentant());
		
		try  {
			UpdateQuery.executeUpdate();
		}catch(SQLException e) {
		}
		cn.DeconnectedBDD(Connect, UpdateQuery);
	}
	
	public ArrayList<Representant> getAllRepresentants() throws SQLException, ClassNotFoundException {
		Connection Connect = cn.ConnectedBDD();
		ResultSet rs;
		String sql = "SELECT * FROM Representant";
		PreparedStatement SelectQuery = Connect.prepareStatement(sql);
		rs = SelectQuery.executeQuery();
		
		while (rs.next()) {
			Representant rep = new Representant(rs.getString("nom"),rs.getString("prenom"),rs.getInt("num"),rs.getString("login"),rs.getString("password"));
			all_representants.add(rep);
		}
		cn.DeconnectedBDD(Connect, SelectQuery);
		return all_representants;
	}
}
